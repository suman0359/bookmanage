
   <?php   $c = $this->uri->rsegment(1) ;  //echo $c; ?>
    
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <?php  
                   
                       $name=$this->session->userdata("username");
                       $user_type=$this->session->userdata("user_type");
                       $id=$this->session->userdata("uid");
                       $permission_department=$this->session->userdata('permissiond');
                       $userinfo=$this->CM->getwhere('user',array('id'=>$id));
                    ?>
                    
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php  echo base_url(""); ?>uploads/<?php echo $userinfo->image ;?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php  echo $name; ?></p>

                            <a href="<?php echo base_url(); ?>"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="<?php if($c=='home')echo "active"  ?>">
                            <a href="<?php echo base_url(); ?>home">
                                <i class="fa fa-home"></i> <span>Home</span>
                            </a>
                        </li>
                        
												
                        <li class="<?php if($c=='user')echo "active"  ?>">
                            <a href="<?php echo base_url(); ?>user">
                                <i class="fa fa-user"></i> <span>User</span>
                            </a>
                        </li>         
                        
                        
                        <li class="<?php if($c=='division')echo "active"  ?>">
                            <a href="<?php echo base_url(); ?>division">
                                <i class="fa fa-user"></i> <span>Division</span>
                            </a>
                        </li>

                        <li class="<?php if($c=='jonal')echo "active"  ?>">
                            <a href="<?php echo base_url(); ?>jonal">
                                <i class="fa fa-user"></i> <span>Jonal</span>
                            </a>
                        </li>
   
                        
                        <li class="<?php if($c=='college')echo "active"  ?>">
                            <a href="<?php echo base_url(); ?>college">
                                <i class="fa fa-user"></i> <span>College</span>
                            </a>
                        </li>

                        <li class="<?php if($c=='books')echo "active"  ?>">
                            <a href="<?php echo base_url(); ?>books">
                                <i class="fa fa-user"></i> <span>Books</span>
                            </a>
                        </li>

                        <li class="<?php if($c=='teachers')echo "active"  ?>">
                            <a href="<?php echo base_url(); ?>teachers">
                                <i class="fa fa-user"></i> <span>Teachers</span>
                            </a>
                        </li>
                         
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
