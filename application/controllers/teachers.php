<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teachers extends CI_Controller 
{
	 

	public $uid;
    public $module;

    public function __construct() {
    parent::__construct();

    $this->load->model('Commons', 'CM') ;  
    $this->module='user';
    $this->uid=$this->session->userdata('uid');
    }

    public function index()
    {
    	$data['teachers_list']=$this->CM->getTotalALL('teachers');
        
        
    	$this->load->view('teachers/index', $data);
    }

    public function add()
    {
      if( !$this->CM->checkpermission($this->module,'add', $this->uid))
             redirect ('error/accessdeny');
      
        
        $data['department_list']=$this->CM->getAll('department', 'name ASC' );
        $data['college_list']=$this->CM->getAll('college', 'name ASC');
        

        $data['name'] = "";
        
      
        $this->load->library('form_validation');


        $this->form_validation->set_rules('name', 'required', 'address', 'college_id', 'department_id');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('teachers/form', $data); 
        }
        else
        {
            
            $datas['name'] = $this->input->post('name');
            $datas['college_id'] = $this->input->post('college_id');
            $datas['dep_id'] = $this->input->post('department_id');
            $datas['address'] = $this->input->post('address');

            $datas['status'] = 1;
            //$datas['entryby']=$this->session->userdata('uid');       
            

            $insert = $this->CM->insert('teachers',$datas) ; 
            if($insert)
            {
                $msg = "Operation Successfull!!";
        		$this->session->set_flashdata('success', $msg);
                redirect('teachers'); 
            }
            else 
            {
                $msg = "There is an error, Please try again!!";
        		$this->session->set_flashdata('error', $msg);
        		$this->load->view('college/form', $data); 
            }
              redirect('teachers','refresh'); 
        }
        
    }

    public function edit($id)
    {
         if( !$this->CM->checkpermission($this->module,'edit', $this->uid))
             redirect ('error/accessdeny');
        
        $content = $this->CM->getInfo('teachers', $id) ; 
        $data['department_list']=$this->CM->getAll('department', 'name', 'ASC');
        $data['college_list']=$this->CM->getAll('college', 'name', 'ASC');
        
        $data['name'] = $content->name;
        $data['address'] = $content->address;
        $data['college_id'] = $content->college_id;
        $data['department_id'] = $content->dep_id;
       // $data['_id'] = $content->district_id;
        
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules( 'name', 'required', 'address', 'college_id', 'department_id');
        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('teachers/form', $data); 
        }
        else
        {
            $datas['name'] = $this->input->post('name');
            $datas['college_id'] = $this->input->post('college_id');
            $datas['dep_id'] = $this->input->post('department_id');
            $datas['address'] = $this->input->post('address');
            //$datas['entryby']=$this->session->userdata('uid');       
 
                if($this->CM->update('teachers', $datas, $id)){
                    $msg = "Operation Successfull!!";
                    $this->session->set_flashdata('success', $msg);
                    redirect('teachers'); 
                }
        }
        
    }
}