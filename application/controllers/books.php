<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Books extends CI_Controller 
{
	 

	public $uid;
    public $module;

    public function __construct() {
    parent::__construct();

    $this->load->model('Commons', 'CM') ;  
    $this->module='user';
    $this->uid=$this->session->userdata('uid');
    }

    public function index()
    {
    	$data['books_list']=$this->CM->getTotalALL('books');
       
        
    	$this->load->view('books/index', $data);
    }

    public function add()
    {
      if( !$this->CM->checkpermission($this->module,'add', $this->uid))
             redirect ('error/accessdeny');
      
       

        $data['group_list']=$this->CM->getAll('group', 'name ASC' );
        
        

        $data['name'] = "";
        
      
        $this->load->library('form_validation');


        $this->form_validation->set_rules('name', 'required', 'address');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('books/form', $data); 
        }
        else
        {
            
            $datas['book_name'] = $this->input->post('book_name');
            $datas['group_id'] = $this->input->post('group_id');
            $datas['book_code'] = $this->input->post('book_code');
            $datas['rate'] = $this->input->post('rate');

            $datas['status'] = 1;
            //$datas['entryby']=$this->session->userdata('uid');       
            

            $insert = $this->CM->insert('books',$datas) ; 
            if($insert)
            {
                $msg = "Operation Successfull!!";
        		$this->session->set_flashdata('success', $msg);
                redirect('books'); 
            }
            else 
            {
                $msg = "There is an error, Please try again!!";
        		$this->session->set_flashdata('error', $msg);
        		$this->load->view('college/form', $data); 
            }
              redirect('books','refresh'); 
        }
        
    }

    public function edit($id)
    {
         if( !$this->CM->checkpermission($this->module,'edit', $this->uid))
             redirect ('error/accessdeny');
        
        $content = $this->CM->getInfo('books', $id) ; 
        $data['group_list']=$this->CM->getAll('group', 'name ASC' );
        
        $data['name'] = $content->book_name;
        $data['group_id'] = $content->group_id;
        $data['book_code'] = $content->book_code;
        $data['book_rate'] = $content->rate;
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules( 'name', 'required', 'address');
        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('books/form', $data); 
        }
        else
        {
            $datas['book_name'] = $this->input->post('book_name');
            $datas['group_id'] = $this->input->post('group_id');
            $datas['book_code'] = $this->input->post('book_code');
            $datas['rate'] = $this->input->post('rate');
            //$datas['entryby']=$this->session->userdata('uid');       
 
                if($this->CM->update('books', $datas, $id)){
                    $msg = "Operation Successfull!!";
                    $this->session->set_flashdata('success', $msg);
                    redirect('books'); 
                }
        }
        
    }
}