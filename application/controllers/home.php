<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->checklogin() ;
    }
    
    public function index()
    {
     
        $datav = "";
    	$this->load->view('home/index', $datav);
    }


    public function getthana($did)
    {
    	$did = trim($did); 
    	$thanalist=$this->CM->getAllWhere('thana', array('district_id'=> $did)) ; 
    	// echo json_encode($thanalist) ; 
    	$html = "<option value=''>Select a thana</option>";
    	foreach ($thanalist as $key => $value) {
    		 $html.="<option value='{$value['id']}'>{$value['name']}</option>"; 
    	}


    	echo $html ; 

    }
    
    
    
    
}


