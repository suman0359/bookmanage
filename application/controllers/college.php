<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class College extends CI_Controller 
{
	 

	public $uid;
    public $module;

    public function __construct() {
    parent::__construct();

    $this->load->model('Commons', 'CM') ;  
    $this->module='user';
    $this->uid=$this->session->userdata('uid');
    }

    public function index()
    {
    	$data['college_list']=$this->CM->getTotalALL('college');
        $data['district_list']=$this->CM->getTotalALL('district');
        $data['thana_list']=$this->CM->getTotalALL('thana');
        
    	$this->load->view('college/index', $data);
    }

    public function add()
    {
      if( !$this->CM->checkpermission($this->module,'add', $this->uid))
             redirect ('error/accessdeny');
      
        //$data['id'] = $this->CM->getMaxID('user'); 
        //$data['department_list']=$this->CM->getAll('department');

        $data['district_list']=$this->CM->getAll('district', 'name ASC' );
        $data['thana_list']=$this->CM->getAll('thana', 'name ASC');
        
        

        $data['name'] = "";
        
      
        $this->load->library('form_validation');


        $this->form_validation->set_rules('name', 'required', 'address');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('college/form', $data); 
        }
        else
        {
            
            $datas['name'] = $this->input->post('name');
            $datas['district_id'] = $this->input->post('district_id');
            $datas['thana_id'] = $this->input->post('thana_id');
            $datas['address'] = $this->input->post('address');

            $datas['status'] = 1;
            //$datas['entryby']=$this->session->userdata('uid');       
            

            $insert = $this->CM->insert('college',$datas) ; 
            if($insert)
            {
                $msg = "Operation Successfull!!";
        		$this->session->set_flashdata('success', $msg);
                redirect('college'); 
            }
            else 
            {
                $msg = "There is an error, Please try again!!";
        		$this->session->set_flashdata('error', $msg);
        		$this->load->view('college/form', $data); 
            }
              redirect('college','refresh'); 
        }
        
    }

    public function edit($id)
    {
         if( !$this->CM->checkpermission($this->module,'edit', $this->uid))
             redirect ('error/accessdeny');
        
        $content = $this->CM->getInfo('college', $id) ; 
        $data['district_list']=$this->CM->getAll('district', 'name', 'ASC');
        $data['thana_list']=$this->CM->getAll('thana', 'name', 'ASC');
        
        $data['name'] = $content->name;
        $data['district_id'] = $content->district_id;
        
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules( 'name', 'required', 'address');
        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('college/form', $data); 
        }
        else
        {
            $datas['name'] = $this->input->post('name'); 
            $datas['district_id'] = $this->input->post('district_id');
            $datas['thana_id'] = $this->input->post('thana_id');
            $datas['address'] = $this->input->post('address');
            //$datas['entryby']=$this->session->userdata('uid');       
 
                if($this->CM->update('college', $datas, $id)){
                    $msg = "Operation Successfull!!";
                    $this->session->set_flashdata('success', $msg);
                    redirect('college'); 
                }
        }
        
    }
}