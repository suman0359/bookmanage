<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Division extends CI_Controller 
{
    public $uid;
    public $module;

    public function __construct() {
    parent::__construct();

    $this->load->model('Commons', 'CM') ;  
    $this->module='user';
    $this->uid=$this->session->userdata('uid');
    }

    public function index()
    {
        $data['division_list']=$this->CM->getAll('division');
        $this->load->view('division/index', $data);
    }

    public function add()
    {
      if( !$this->CM->checkpermission($this->module,'add', $this->uid))
             redirect ('error/accessdeny');
      
        $data['id'] = $this->CM->getMaxID('user'); 
        $data['department_list']=$this->CM->getAll('department');
        
        $data['name'] = "";
        
      
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('division/form', $data); 
        }
        else
        {
            
            $datas['name'] = $this->input->post('name'); 
            
            $datas['status'] = 1;
            //$datas['entryby']=$this->session->userdata('uid');       
            

            $insert = $this->CM->insert('division',$datas) ; 
            if($insert)
            {
                $msg = "Operation Successfull!!";
                $this->session->set_flashdata('success', $msg);
                redirect('division'); 
            }
            else 
            {
                $msg = "There is an error, Please try again!!";
                $this->session->set_flashdata('error', $msg);
                $this->load->view('division/form', $data); 
            }
              redirect('division','refresh'); 
        }
        
    }


    public function edit($id)
    {
         if( !$this->CM->checkpermission($this->module,'edit', $this->uid))
             redirect ('error/accessdeny');
        
        $content = $this->CM->getInfo('division', $id) ; 
        $data['department_list']=$this->CM->getAll('department');
       
        
        $data['name'] = $content->name;
        
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules( 'name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('division/form', $data); 
        }
        else
        {
            $datas['name'] = $this->input->post('name'); 
            
            //$datas['entryby']=$this->session->userdata('uid');       
 
                if($this->CM->update('division', $datas, $id)){
                    $msg = "Operation Successfull!!";
                    $this->session->set_flashdata('success', $msg);
                    redirect('division'); 
                }
        }
        
}


}