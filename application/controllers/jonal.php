<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jonal extends CI_Controller 
{
	public $uid;
	public $module;

	public function __construct() {
	parent::__construct();

	$this->load->model('Commons', 'CM') ;  
	$this->module='user';
	$this->uid=$this->session->userdata('uid');
    }

    public function index()
    {
    	$data['jonal_list']=$this->CM->getTotalALL('jonal');
        
    	$this->load->view('jonal/index', $data);
    }

    public function add()
    {
      if( !$this->CM->checkpermission($this->module,'add', $this->uid))
             redirect ('error/accessdeny');
      
        //$data['id'] = $this->CM->getMaxID('user'); 
        //$data['department_list']=$this->CM->getAll('department');

        $data['division_list']=$this->CM->getTotalALL('division');
        
        $data['name'] = "";
        
      
        $this->load->library('form_validation');


        $this->form_validation->set_rules('name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('jonal/form', $data); 
        }
        else
        {
            
            $datas['name'] = $this->input->post('name'); 
            
            $datas['status'] = 1;
            //$datas['entryby']=$this->session->userdata('uid');       
            

            $insert = $this->CM->insert('jonal',$datas) ; 
            if($insert)
            {
                $msg = "Operation Successfull!!";
        		$this->session->set_flashdata('success', $msg);
                redirect('jonal'); 
            }
            else 
            {
                $msg = "There is an error, Please try again!!";
        		$this->session->set_flashdata('error', $msg);
        		$this->load->view('jonal/form', $data); 
            }
              redirect('jonal','refresh'); 
        }
        
    }

    public function edit($id)
    {
         if( !$this->CM->checkpermission($this->module,'edit', $this->uid))
             redirect ('error/accessdeny');
        
        $content = $this->CM->getInfo('jonal', $id) ; 
        $data['division_list']=$this->CM->getTotalALL('division');
       
        
        $data['name'] = $content->name;
        $data['division_id'] = $content->div_id;
        
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules( 'name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('jonal/form', $data); 
        }
        else
        {
            $datas['name'] = $this->input->post('name'); 
            $datas['div_id'] = $this->input->post('division_id');
            
            //$datas['entryby']=$this->session->userdata('uid');       
 
                if($this->CM->update('jonal', $datas, $id)){
                    $msg = "Operation Successfull!!";
                    $this->session->set_flashdata('success', $msg);
                    redirect('jonal'); 
                }
        }
        
    }
}